<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\User;

class AuthController extends Controller
{
    //
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->all()], 422, [], JSON_NUMERIC_CHECK);
        }
        $request['password'] = Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);
        $user = User::create($request->toArray());
        if ($user->email_verified_at == null) {
            $user->sendEmailVerificationNotification();
            $response = ["message" => "Verify Your Email First to login"];
            return response()->json($response, 422, [], JSON_NUMERIC_CHECK);
        } else {
            $token = $user->createToken('Laravel Password Grant Client')->accessToken;
            $response = ['token' => $token, 'user_id' => $user->id];
            return response()->json($response, 200, [], JSON_NUMERIC_CHECK);
        }
    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->all(), 'num' => "10.1"], 422, [], JSON_NUMERIC_CHECK);
        }
        $user = User::where('email', $request->email)->first();
        if ($user) {
             if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                if ($user->email_verified_at == null) {
                    $response = ["message" => "Email is not verifed" ,'token' => $token];
                    return response()->json($response, 422, [], JSON_NUMERIC_CHECK);
                } else{
                    $response = ['token' => $token, 'user' => $user];
                    return response()->json($response, 200, [], JSON_NUMERIC_CHECK);
                }

            } else {
                $response = ["message" => "Password mismatch"];
                return response()->json($response, 422, [], JSON_NUMERIC_CHECK);
            }
        } else {
            $response = ["message" => 'User does not exist'];
            return response()->json($response, 422, [], JSON_NUMERIC_CHECK);
        }
    }


    public function logout(Request $request)
    {
        $token = $request->user()->token();
        // dd($request->user()->name);
        $token->revoke();
        $response = ['message' => 'You have been successfully logged out!'];
        return response()->json($response, 200, [], JSON_NUMERIC_CHECK);
    }

    public function verify($user_id,$hash,  Request $request) {
        if (!$request->hasValidSignature()) {
            return response()->json(["message" => "Invalid/Expired url provided."], 401);
        }

        $user = User::findOrFail($user_id);

        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
        }

        return response()->json(["message" => "Email Address verified."], 200);
    }

    public function resend(Request $request) {
        if ($request->user()->hasVerifiedEmail()) {
            return response()->json(["message" => "Email already verified."], 400);
        }

        $request->user()->sendEmailVerificationNotification();

        return response()->json(["message" => "Email verification link sent on your email id"]);
    }
}
