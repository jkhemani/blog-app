<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //

    public function getUsers(Request $request)
    {
        if ($request->has('user_id')) {
            if ($request->has('except')) {
                $user_id = $request->except;
                $users = User::where('id', '!=',  $user_id)->where('email_verified_at', '!=', null)->get();
            } else {
                $user_id = $request->user_id;
                $users = User::where('id', $user_id)->where('email_verified_at', '!=', null)->get();
            }

            // $user_id = $request->user_id;
            // $users = User::where('id', $user_id)->get();

            $response = ["message" => "Success", 'users' => $users];
            $response_code = 200;
        } else {
            $users = User::where('email_verified_at', '!=', null)->get();

            $response = ["message" => "Success", 'users' => $users];
            $response_code = 200;
        }
        return response()->json($response, $response_code, [], JSON_NUMERIC_CHECK);
    }
}
