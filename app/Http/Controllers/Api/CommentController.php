<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\blogComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;

class CommentController extends Controller
{
    public function store(Request $request)
    {

        // if ($request->has('category_id') && $request->has('title') && $request->has('body') && $request->has('thumbnail') && $request->has('visibility') && $request->has('status')) {
        $validator = Validator::make($request->all(), [
            'blog_id' => 'required',
            'user_id' => 'required',
            'comment' => 'required',
        ]);
        if ($validator->fails()) {
            $response = ["message" => $validator->messages()->first(),];
            $response_code = 400;
        } else {


            $comment = new blogComment;
            $comment->comment = $request->comment;
            $comment->blog_id = $request->blog_id;
            $comment->user_id = $request->user_id;
            $comment->created_at = Carbon::now()->toDateTimeString();
            $comment->updated_at = Carbon::now()->toDateTimeString();
            $comment->save();



            $response = ["message" => "Success", 'comment_id' => $comment->id, ];
            $response_code = 200;
        }
        return response()->json($response, $response_code, [], JSON_NUMERIC_CHECK);
    }
}
