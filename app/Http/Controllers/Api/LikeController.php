<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\blogLike;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;

class LikeController extends Controller
{
    public function store(Request $request)
    {

        // if ($request->has('category_id') && $request->has('title') && $request->has('body') && $request->has('thumbnail') && $request->has('visibility') && $request->has('status')) {
        $validator = Validator::make($request->all(), [
            'blog_id' => 'required',
            'user_id' => 'required',
        ]);
        if ($validator->fails()) {
            $response = ["message" => $validator->messages()->first(),];
            $response_code = 422;
        } else {

            $check_like = blogLike::where('blog_id' , $request->blog_id)
            ->where('user_id' , $request->user_id)
            ->get();

            if(count($check_like) > 0){
                $like = blogLike::where('blog_id' , $request->blog_id)
                ->where('user_id' , $request->user_id)
                ->delete();
            }else{
                $like = new blogLike;
                $like->blog_id = $request->blog_id;
                $like->user_id = $request->user_id;
                $like->created_at = Carbon::now()->toDateTimeString();
                $like->updated_at = Carbon::now()->toDateTimeString();
                $like->save();
            }




            $response = ["message" => "Success", 'like' => $like, ];
            $response_code = 200;
        }
        return response()->json($response, $response_code, [], JSON_NUMERIC_CHECK);
    }
}
