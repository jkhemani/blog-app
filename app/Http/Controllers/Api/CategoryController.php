<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\blogCategory;
use App\Models\blogComment;
use App\Models\blogLike;
use App\Models\blogPrivateUser;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class CategoryController extends Controller
{

    public function getCategory(Request $request)
    {
        if($request->has('single_page')){
            $single_page = $request->single_page;
        }else{
            $single_page = 10;
        }
        if($request->has('search')){
            $search = $request->search;
            $categories = blogCategory::where('blog_category','ilike', '%'.$search.'%')->paginate($single_page);

            $response = ["message" => "Success", 'categories' => $categories];
            $response_code = 200;
        }else if ($request->has('parent_id')) {
            $root = $request->parent_id;
            if ($request->has('all')) {
                $categories = blogCategory::where('parent_id', $root)->get();
            }else{
                $categories = blogCategory::where('parent_id', $root)->paginate($single_page);
            }


            $response = ["message" => "Success", 'categories' => $categories];
            $response_code = 200;
        }else if ($request->has('id')) {
            $root = $request->id;
            $categories = blogCategory::where('id', $root)->paginate($single_page);
            if($categories->first()->parent_id == 0){
                $categories = blogCategory::where('parent_id', 0)->paginate($single_page);
            }

            $response = ["message" => "Success", 'categories' => $categories];
            $response_code = 200;
        }
        else {
            $categories = blogCategory::get();

            $response = ["message" => "Success", 'categories' => $categories];
            $response_code = 200;
        }
        return response()->json($response, $response_code, [], JSON_NUMERIC_CHECK);
    }
    public function store(Request $request)
    {

        if ($request->has('parent_id') && $request->has('category')) {

            $category = blogCategory::create([
                'blog_category' => $request->category,
                'parent_id' => $request->parent_id,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]);
            // $category = new blogCategory;
            // $category->blog_category = $request->category;
            // $category->parent_id = $request->parent_id;
            // $category->created_at = Carbon::now()->toDateTimeString();
            // $category->updated_at = Carbon::now()->toDateTimeString();
            // $category->save();

            $response = ["message" => "Success", 'category' => $category];
            $response_code = 200;
        } else {
            $response = ["message" => "Required Parameter missing"];
            $response_code = 422;
        }
        return response()->json($response, $response_code, [], JSON_NUMERIC_CHECK);
    }
    public function delete(Request $request)
    {

        if ($request->has('category_id')) {
            $id = $request->category_id;
            $category = blogCategory::where('id', $id)->get();
            if (count($category) > 0) {
                while (count($category) > 0) {
                    // dd($category);
                    $parent_id = $category->first()->id;
                    // dd($parent_id);
                    $category_del = blogCategory::where('id', $category->first()->id)->delete();

                    // $blog_del = Blog::where('category_id', $category->first()->id)->delete();
                    $blog = Blog::where('category_id', $parent_id)->get();
                    if (count($blog) > 1) {
                        $blog_id = $blog->first()->id;
                        blogPrivateUser::where('blog_id', $blog_id)->delete();
                        blogComment::where('blog_id', $blog_id)->delete();
                        blogLike::where('blog_id', $blog_id)->delete();

                        // $blog->private()->detach();
                        // $blog->comments()->detach();
                        // $blog->likes()->detach();
                        // $admin->roles()->detach();
                        // if (!Admin::destroy($del_id)) {
                        $blog->delete();
                    }
                    if ($category_del) {
                        $category = blogCategory::where('parent_id', $parent_id)->get();
                    }
                }

                // $category = blogCategory::where('id', $id)->delete();
                $response = ["message" => "Success", 'category' => $category];
                $response_code = 200;
            } else {
                $response = ["message" => "Category not found"];
                $response_code = 422;
            }
        } else {
            $response = ["message" => "Required Parameter missing"];
            $response_code = 422;
        }
        return response()->json($response, $response_code, [], JSON_NUMERIC_CHECK);
    }
    public function update(Request $request)
    {

        if ($request->has('category_id') && $request->has('category')) {
            $id = $request->category_id;

            $category = blogCategory::where('id', $id)->update([
                'blog_category' => $request->category,

                'updated_at' => Carbon::now()->toDateTimeString(),
            ]);
            $response = ["message" => "Success", 'category' => $category];
            $response_code = 200;
        } else {
            $response = ["message" => "Required Parameter missing"];
            $response_code = 422;
        }
        return response()->json($response, $response_code, [], JSON_NUMERIC_CHECK);
    }
}
