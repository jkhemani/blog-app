<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\blogComment;
use App\Models\blogLike;
use App\Models\blogPrivateUser;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;

class BlogController extends Controller
{
    //
    public function store(Request $request)
    {

        // if ($request->has('category_id') && $request->has('title') && $request->has('body') && $request->has('thumbnail') && $request->has('visibility') && $request->has('status')) {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image:jpeg,png,jpg|max:2048',
            'category_id' => 'required',
            'title' => 'required',
            'body' => 'required',
            // 'thumbnail' => 'required',
            'visibility' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            $response = ["message" => $validator->messages()->first(),];
            $response_code = 400;
        } else {
            if ($request->category_id == 0) {
                $response = ["message" => "Please Select a Category",];
                $response_code = 422;
            } else {
                $uploadFolder = 'blog_thumbnails';
                // if ($request->has('image')) {
                $image = $request->file('image');
                $image_uploaded_path = $image->store($uploadFolder, 'public');

                $blog = new Blog;
                $blog->title = $request->title;
                $blog->body = $request->body;
                $blog->thumbnail = basename($image_uploaded_path);
                $blog->visibility = $request->visibility;
                $blog->user_id = $request->user_id;
                $blog->status = $request->status;
                $blog->category_id = $request->category_id;

                $blog->created_at = Carbon::now()->toDateTimeString();
                $blog->updated_at = Carbon::now()->toDateTimeString();
                $blog->save();


                if ($request->visibility == "2") {
                    if ($request->has('authers')) {
                        $authers = (explode(",", $request->authers));
                        // echo ($authers);
                        foreach ($authers as $auther) {
                            # code...
                            $private_users = blogPrivateUser::create([
                                'blog_id' => $blog->id,
                                'user_id' => $auther,
                            ]);
                        }
                    }
                    $response = ["message" => "Success", 'blog_id' => $blog->id, 'authers' => $private_users];
                    $response_code = 200;
                } else {
                    $response = ["message" => "Success", 'blog_id' => $blog->id];
                    $response_code = 200;
                }
                // } else {
                //     // echo $request->visibility;
                //     $blog = Blog::create([
                //         'title' => $request->title,
                //         'body' => $request->body,
                //         'thumbnail' => '',
                //         'visibility' => $request->visibility,
                //         'user_id' => $request->user_id,
                //         'status' => $request->status,
                //         'category_id' => $request->category_id,

                //         'created_at' => Carbon::now()->toDateTimeString(),
                //         'updated_at' => Carbon::now()->toDateTimeString(),
                //     ]);
                // }
                // $uploadedImageResponse = array(
                //     "image_name" => basename($image_uploaded_path),
                //     "image_url" => Storage::disk('public')->url($image_uploaded_path),
                //     "mime" => $image->getClientMimeType()
                // );


            }
        }
        return response()->json($response, $response_code, [], JSON_NUMERIC_CHECK);
    }

    public function update(Request $request)
    {

        // if ($request->has('category_id') && $request->has('title') && $request->has('body') && $request->has('thumbnail') && $request->has('visibility') && $request->has('status')) {
        $validator = Validator::make($request->all(), [
            'blog_id' => 'required',
            'image' => 'image:jpeg,png,jpg|max:2048',
            'category_id' => 'required',
            'title' => 'required',
            'body' => 'required',
            // 'thumbnail' => 'required',
            'chg_image' => 'required',
            'visibility' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            $response = ["message" => $validator->messages()->first(),];
            $response_code = 400;
        } else {
            if ($request->category_id == 0) {
                $response = ["message" => "Please Select a Category",];
                $response_code = 422;
            } else {

                if ($request->chg_image == "true") {
                    $file = $request->only('image_name');
                    // echo public_path();
                    if (is_file(public_path() . '/storage/blog_thumbnails/'. $file['image_name'])) {
                        if (unlink(public_path() . '/storage/blog_thumbnails/'.$file['image_name'])) {
                            $uploadFolder = 'blog_thumbnails';
                            // if ($request->has('image')) {
                            $image = $request->file('image');
                            $image_uploaded_path = $image->store($uploadFolder, 'public');
                            $image_name = basename($image_uploaded_path);
                        } else {
                            return response()->json(['error' => true, 'msg' => 'Some Error Occurred']);
                        }
                    } else {
                        return response()->json(['error' => true, 'msg' => 'File not Exists']);
                    }
                } else {
                    $image_name = $request->image_name;
                }


                $blog = Blog::find($request->blog_id);
                $blog->title = $request->title;
                $blog->body = $request->body;
                $blog->thumbnail = $image_name;
                $blog->visibility = $request->visibility;
                $blog->user_id = $request->user_id;
                $blog->status = $request->status;
                $blog->category_id = $request->category_id;
                $blog->save();

                blogPrivateUser::where('blog_id' , $blog->id)->delete();


                if ($request->visibility == "2") {
                    if ($request->has('authers')) {

                        $authers = (explode(",", $request->authers));
                        // echo ($authers);
                        foreach ($authers as $auther) {
                            # code...
                            $private_users = blogPrivateUser::create([
                                'blog_id' => $blog->id,
                                'user_id' => $auther,
                            ]);
                        }
                    }
                    $response = ["message" => "Success", 'blog_id' => $blog->id, 'authers' => $private_users];
                    $response_code = 200;
                } else {
                    $response = ["message" => "Success", 'blog_id' => $blog->id];
                    $response_code = 200;
                }
                // } else {
                //     // echo $request->visibility;
                //     $blog = Blog::create([
                //         'title' => $request->title,
                //         'body' => $request->body,
                //         'thumbnail' => '',
                //         'visibility' => $request->visibility,
                //         'user_id' => $request->user_id,
                //         'status' => $request->status,
                //         'category_id' => $request->category_id,

                //         'created_at' => Carbon::now()->toDateTimeString(),
                //         'updated_at' => Carbon::now()->toDateTimeString(),
                //     ]);
                // }
                // $uploadedImageResponse = array(
                //     "image_name" => basename($image_uploaded_path),
                //     "image_url" => Storage::disk('public')->url($image_uploaded_path),
                //     "mime" => $image->getClientMimeType()
                // );


            }
        }
        return response()->json($response, $response_code, [], JSON_NUMERIC_CHECK);
    }


    public function publishBlogs(Request $request)
    {
        if ($request->has('blog_id')) {
            $id = $request->blog_id;

            $blog = Blog::where('id', $id)->update([
                'published_at' => Carbon::now()->toDateTimeString(),
                'status' => 2

            ]);
            $response = ["message" => "Success", 'blog' => $blog];
            $response_code = 200;
        } else {
            $response = ["message" => "Required Parameter missing"];
            $response_code = 422;
        }
        return response()->json($response, $response_code, [], JSON_NUMERIC_CHECK);
    }

    public function inactive(Request $request)
    {

        if ($request->has('blog_id')) {
            $id = $request->blog_id;

            $blog = Blog::where('id', $id)->update([

                'status' => 3,
                'updated_at' => Carbon::now()->toDateTimeString(),

            ]);

            $response = ["message" => "Success", 'blog' => $blog];
            $response_code = 200;
        } else {
            $response = ["message" => "Required Parameter missing"];
            $response_code = 422;
        }
        return response()->json($response, $response_code, [], JSON_NUMERIC_CHECK);
    }
    public function getBlogs(Request $request)
    {
        if($request->has('single_page')){
            $single_page = $request->single_page;
        }else{
            $single_page = 10;
        }
        if($request->has('search')){
            $search = $request->search;
        }else{
            $search = '';
        }
        if ($request->has('blog_id')) {
            $blog_id = $request->blog_id;
            $user_id = $request->user_id;
            $blogs = Blog::with(
                array(
                    'private',
                    'auther',
                    'category',
                    'comments' => function ($query) {
                        $query->with('auther');
                        $query->orderBy('created_at', 'DESC');
                    },
                    'likes' => function ($query) {
                        // $query->with('auther');
                    }
                )
                // ['auther', 'category','comments' , 'likes']
            )
                ->where('id', $blog_id)
                ->get();




            if ($blogs) {
                $check_like = blogLike::where('blog_id', $request->blog_id)
                    ->where('user_id', $request->user_id)
                    ->get();

                if ($blogs->first()->visibility != 1 && $blogs->first()->user_id != $user_id) {
                    if (!$request->has('edit')) {
                        $check = blogPrivateUser::where('user_id', $user_id)
                            ->where('blog_id', $blog_id)
                            ->get();
                        // dd($check);
                        if (count($check) > 0) {
                            $response = ["message" => "Success", 'blogs' => $blogs, 'is_liked' => $check_like];
                            $response_code = 200;
                        } else {
                            $response = ["message" => "Not Permitted",];
                            $response_code = 404;
                        }
                    } else {
                        $response = ["message" => "Not Permitted",];
                        $response_code = 404;
                    }
                } else if ($blogs->first()->status != 2 && $blogs->first()->user_id != $user_id) {
                    $response = ["message" => "Not Permitted",];
                    $response_code = 404;
                } else {
                    $response = ["message" => "Success", 'blogs' => $blogs, 'is_liked' => $check_like];
                    $response_code = 200;
                }
            }
        } else if (!$request->has('all')) {
            $user_id = $request->user_id;
            $blogs = Blog::where('user_id', $user_id);
            if($search != ""){
                $blogs = $blogs->where('title', 'ilike', '%'.$search.'%')
                ->orWhere('body', 'like', '%'.$search.'%')
                ->orWhere('id', 'like', '%'.$search.'%');
            }
            $blogs = $blogs->paginate($single_page);

            $response = ["message" => "Success", 'blogs' => $blogs];
            $response_code = 200;
        } else if ($request->has('user_id')) {
            $user_id = $request->user_id;


            // $blogs = Blog::where('visibility', 1)->get();
            $blogs = Blog::with(
                array(
                    'auther',
                    'category',
                    'comments' => function ($query) {
                        $query->with('auther');
                    },
                    'likes' => function ($query) {
                        $query->with('auther');
                    }
                )
                // ['auther', 'category','comments' , 'likes']
            )
                ->whereHas('private', function ($query) use ($request) {
                    if ($request->has('user_id'))
                        $query->where('user_id', $request->user_id);
                })
                ->orDoesntHave('private')

                ->where('status', 2)
                ->orderBy('published_at', 'Desc')

                ->get();

            // $blogs2 = Blog::with(['auther' , 'category'])
            // ->doesntHave('private')
            // ->where('visibility' , 1)
            // ->where('status' , 2)
            // ->orderBy('id' ,'Desc')
            // ->get();

            // $blogs = $blogs1->merge($blogs2);
            // $private_blogs = User::get()->privateBlogs;
            // dd($private_blogs);

            $response = ["message" => "Success", 'blogs' => $blogs,'hi'=>9];
            $response_code = 200;
        } else {
            $response = ["message" => "Required Parameter Missing"];
            $response_code = 402;
        }
        return response()->json($response, $response_code, [], JSON_NUMERIC_CHECK);
    }
}
