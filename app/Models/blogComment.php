<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class blogComment extends Model
{
    use HasFactory;
    public function auther()
    {
        return $this->hasMany(User::class ,'id' , 'user_id');
    }
}
