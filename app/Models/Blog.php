<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'body',
        'thumbnail',
        'status',
        'category_id',
        'user_id',
        'published_at'
    ];

    public function auther()
    {
        return $this->hasMany(User::class ,'id' , 'user_id');
    }
    public function comments()
    {
        return $this->hasMany(blogComment::class ,'blog_id' , 'id');
    }
    public function likes()
    {
        return $this->hasMany(blogLike::class ,'blog_id' , 'id');
    }
    public function category()
    {
        return $this->hasMany(blogCategory::class ,'id' , 'category_id');
    }
    public function private()
    {
        return $this->hasMany(blogPrivateUser::class ,'blog_id' , 'id');
    }

}
