<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\BlogController;
use App\Http\Controllers\Api\CommentController;
use App\Http\Controllers\Api\LikeController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::middleware('auth:api')->group(function () {
    // our routes to be protected will go in here
    Route::post('/logout', [AuthController::class , 'logout'])->name('logout');
    Route::post('email/resend', [AuthController::class , 'resend'])->name('verification.resend');
    Route::post('categories/', [CategoryController::class , 'getCategory'])->name('categories');
    Route::post('categories/store', [CategoryController::class , 'store'])->name('categories');
    Route::post('categories/delete', [CategoryController::class , 'delete'])->name('categories');
    Route::post('categories/update', [CategoryController::class , 'update'])->name('categories');
    Route::post('blogs/', [BlogController::class , 'getBlogs'])->name('blogs');
    Route::post('blogs/store', [BlogController::class , 'store'])->name('blogs');
    Route::post('blogs/update', [BlogController::class , 'update'])->name('blogs');
    Route::post('blogs/publish', [BlogController::class , 'publishBlogs'])->name('blogs');
    Route::post('blogs/inactive', [BlogController::class , 'inactive'])->name('blogs');
    Route::post('users/', [UserController::class , 'getUsers'])->name('users');
    Route::post('comments/store', [CommentController::class , 'store'])->name('comments');
    Route::post('likes/store', [LikeController::class , 'store'])->name('likes');
});

Route::group(['middleware' => ['cors', 'json.response']], function () {

    // public routes
    Route::post('/login', [AuthController::class , 'login'])->name('login');
    Route::post('/register',[AuthController::class , 'register'])->name('register');



    // ...
    // ...
});
