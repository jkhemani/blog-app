<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->text('title');
            $table->text('body');
            $table->string('thumbnail');
            $table->integer('status')->comment = '1:Draft , 2:Published , 3:Inavtive';
            $table->bigInteger('category_id');
            $table->integer('visibility')->comment = '1:Public , 2:Private';
            $table->bigInteger('user_id');
            $table->timestamp('published_at', $precision = 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
